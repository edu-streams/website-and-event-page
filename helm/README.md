To create db use sql

    CREATE DATABASE p_website;
    CREATE USER 'u_website'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
    GRANT ALL PRIVILEGES ON p_website.* TO 'u_website'@'%'
module.exports = ({ env }) => ({
  'migrations': {
    enabled: true,
    config: {
      autoStart: true,
      migrationFolderPath : 'migrations'
    },
  },
  'users-permissions': {
    config: {
      jwt: {
        expiresIn: '7d',
      },
    },
  },
  'google-auth': {
    enabled: true,
    resolve: "./node_modules/strapi-google-auth/" // resolves the issue
  },
});

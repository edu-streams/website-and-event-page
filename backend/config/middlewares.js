module.exports = [
  'strapi::errors',
  'strapi::security',
  {
    name: 'strapi::cors',
    config: {
      enabled: true,
      headers: [
        'Content-Type',
        'Authorization',
        'X-Frame-Options',
        'User-Agent',
        'Origin',
        'X-Requested-With',
        'Accept',
      ],
      origin: [
        'http://localhost:3000',
        'http://localhost:1337',
        'https://api-stage.edu-streams.org',
        'https://stage.edu-streams.org',
        'https://api.edu-streams.org',
        'https://edu-streams.org'
      ],
    },
  },
  // 'strapi::poweredBy',
  'strapi::logger',
  'strapi::query',
  'strapi::body',
  'strapi::session',
  'strapi::favicon',
  'strapi::public',
];

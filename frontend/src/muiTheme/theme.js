import { createTheme } from '@mui/material/styles';

const COLOR = {
    green: '#16A34A',
    darkGreen: '#128d3f',
    white: '#FFFFFF',
    gray: '#71717A',
    black: '#181B19',
    lightBlack: '#181B19',
    lightBlue: '#edf5f5',
};

const theme = createTheme({
    breakpoints: {
        values: {
            xs:0,
            sm: 320,
            md: 744,
            lg: 1024,
            xl: 1280,
        },
    },

    palette: {
        primary: {
            main: '#111111',
            light: '#71717A',
            dark: '#71717A',
        },
        secondary: {
            main: '#f3672b',
            light: '#fc7b43',
        },
        text: {
            black: '#181B19',
            gray: '#71717A',
            white: '#ffffff',
        },
    },

    typography: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 400,
        fontSize: '12px',
        lineHeight: 1.5,
        color: COLOR.lightBlack,

        h1: {
            fontFamily: 'Open Sans',
            fontWeight: 700,
            fontSize: '24px',
        },
        h2: {
            fontWeight: 700,
            fontSize: '20px',
            lineHeight: 1.5,
        },
        description: {
            //в герої
            fontWeight: 400,
            fontSize: '14px',
            lineHeight: 1.5,
        },
        text: {
            // li + acord
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '21px',
        },
        subtitle: {
            fontWeight: 700,
            fontSize: '14px',
        },
        li: {
            // footer
            fontWeight: 700,
            fontSize: '12px',
        },
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: ({ ownerState }) => ({
                    ...(ownerState.variant === 'contained' &&
                        ownerState.color === 'primary' && {
                            backgroundColor: COLOR.green,
                            color: COLOR.white,
                            fontWeight: 700,
                            fontSize: 16,
                            lineHeight: 1.5,
                            padding: '10px 18px',
                            borderRadius: '4px',
                            textAlign: 'center',
                            textTransform: 'none',

                            '&:hover': {
                                backgroundColor: COLOR.darkGreen,
                            },
                            '&:focus': {
                                backgroundColor: COLOR.darkGreen,
                                boxShadow: '0px 4px 8px rgba(17, 17, 17, 0.15)',
                            },
                        }),
                }),
            },
        },
    },
});

export default theme;

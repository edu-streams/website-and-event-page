import { Typography, Button } from '@mui/material';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';

import mini from '../../image/macbook_mob.png';
import maxi from '../../image/macbook_tab.png';
import desc from '../../image/macbook_desctop.png';

import css from '../Hero/Hero.module.css';

export const Hero = () => {
    return (
        <div className={css.info_bgr}>
            <div className={css.text_wrapper}>
                <Typography variant="h1" component="h1" sx={{ fontSize: { md: '30px' }, lineHeight: { md: '1.5' } }}>
                    Безперервне навчання завдяки обробці даних у реальному часі
                </Typography>

                <Typography variant="description" component="p" color="text.gray" sx={{ fontSize: { md: '16px' } }}>
                    Перевага потокового навчання - адаптація до великих даних і робота в реальному часі, що важливо для
                    додатків з мінімальною затримкою.
                </Typography>
                <div className={css.btnWrap}>
                    <Button variant="contained" endIcon={<OpenInNewIcon />} sx={{ marginTop: '20px' }}>
                        Зареєструватися
                    </Button>
                </div>
            </div>

            <img src={mini} alt="img" className={css.miniImg} />
            <img src={maxi} alt="img" className={css.maxiImg} />
            <img src={desc} alt="img" className={css.descImg} />
        </div>
    );
};

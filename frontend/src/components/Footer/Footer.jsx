import { IconButton } from '@mui/material';
import { FaTelegramPlane } from 'react-icons/fa';
import { FaInstagramSquare } from 'react-icons/fa';
import { BsDiscord } from 'react-icons/bs';
import { Logo } from '../Logo/Logo';
import css from '../Footer/Footer.module.css';

const iconButtonStyles = {
    color: '#121212',
    '&:hover, &:focus, &:active, &:disabled': {
        color: '#16A34A',
        boxShadow: 'none',
    },
    '& .MuiIconButton-label': {
        transition: 'none',
    },
    '&.MuiIconButton-sizeMedium': {
        size:'medium',
    }
};

export const Footer = () => {
    const goToDiscortd = () => {
        window.open('https://discord.com/', '_blank');
    };

    const goToTelegram = () => {
        window.open('https://web.telegram.org/a/', '_blank');
    };

    const goToInstagram = () => {
        window.open('https://www.instagram.com/', '_blank');
    };

    return (
        <div className={css.section}>
            <div className={css.firstBox}>
                <Logo/>
                <div className={css.iconBox}>
                    <ul className={css.iconList}>
                        <li>
                            <IconButton
                                aria-label="icon"
                                sx={iconButtonStyles}
                                disableTouchRipple={true}
                                onClick={goToDiscortd}
                            >
                                <BsDiscord className={css.iconStyles} />
                            </IconButton>
                        </li>
                        <li>
                            <IconButton
                                aria-label="icon"
                                sx={iconButtonStyles}
                                disableTouchRipple={true}
                                onClick={goToTelegram}
                            >
                                <FaTelegramPlane className={css.iconStyles} />
                            </IconButton>
                        </li>
                        <li>
                            <IconButton
                                aria-label="icon"
                                sx={iconButtonStyles}
                                disableTouchRipple={true}
                                onClick={goToInstagram}
                            >
                                <FaInstagramSquare className={css.iconStyles} />
                            </IconButton>
                        </li>
                    </ul>
                </div>
            </div>
            <ul className={css.list}>
                <li className={css.item}>
                    <a className={css.link} href="/">Головна</a>
                </li>
                <li className={css.item}>
                    <a className={css.link} href="/">Про нас</a>
                </li>
                <li className={css.item}>
                    <a className={css.link} href="/">Контакти</a>
                </li>
                <li className={css.item}>
                    <a className={css.link} href="/">F.A.Q</a>
                </li>
                <li className={css.item}>
                    <a className={css.link} href="/">Конфіденційність</a>
                </li>
            </ul>
            <p className={css.text}>© 2023 EDU STREAMS. All Rights Reserved.</p>
        </div>
    );
};

import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Box, Typography} from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import { useState } from 'react';
import css from './FAQ.module.css';

const CustomExpandIcon = () => {
    const iconStyles = {
        color: '#111111',
        fontSize: '30px',
    };
    return (
        <Box
            sx={{
                '.collapsIconWrapper': {
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: { xs: '9px', md: '11px' },
                },
                '.Mui-expanded & > .collapsIconWrapper': {
                    display: 'none',
                },
                '.expandIconWrapper': {
                    display: 'none',
                },
                '.Mui-expanded & > .expandIconWrapper': {
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: '50px',
                    background: '#EDF5F5',
                    padding: { xs: '9.5px', md: '11.5px' },
                },
            }}
        >
            <div className="expandIconWrapper">
                <ExpandLessIcon sx={iconStyles} />
            </div>
            <div className="collapsIconWrapper">
                <ExpandMoreIcon sx={iconStyles} />
            </div>
        </Box>
    );
};

const QuestionWrapp = {
    marginBottom: '10px',
    boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.06), 0px 1px 3px rgba(0, 0, 0, 0.1)',
    borderRadius: '5px',
    border: '1px solid rgba(17, 17, 17, 0.1)',
    padding: { xs: '10px', md: '13px 10px 13px 10px' },

    '&:before ': {
        content: 'none',
    },
    
};

const SummaryStyles = {
    padding: '0px',
    '&.Mui-expanded': {
        minHeight: '48px',
    },
    '& .MuiAccordionSummary-content': {
        margin: '0px',
    },
    '& .MuiAccordionSummary-content.Mui-expanded': { margin: '0px' },
    '& .Mui-expanded .expandIconWrapper': { padding: '3px', backgroundColor: 'rgba(220, 252, 231, 0.25)'}, 
 // кружечок там де стрілка
};

export const FAQ = () => {
    const [expanded, setExpanded] = useState(false);

    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <Box sx={{padding:'30px 20px 50px 20px', backgroundColor: ' rgba(220, 252, 231, 0.25);' }}>
            <Typography
                variant="h2"
                component="h2"
                color="text.black"
                align="center"
                sx={{ fontSize: { md: '30px', marginBottom: '40px' }}}
            >
                F.A.Q
            </Typography>
            <div className={css.faqSection}>
                <div>
                    <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')} sx={QuestionWrapp}>
                        <AccordionSummary
                            expandIcon={<CustomExpandIcon />}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                            sx={SummaryStyles}
                        >
                            <h3 className={css.questionsTitle}>Які алгоритми популярні в потоковому навчанні?</h3>
                        </AccordionSummary>
                        <AccordionDetails sx={{ padding: '0px', marginBottom: '10px' }}>
                            <ul className={css.questionsList}>
                                <li className={css.questionsItems}>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, quibusdam.
                                    Minima, dolorum non saepe voluptatem a fuga atque esse perspiciatis? Repellendus
                                    impedit consequuntur similique eligendi exercitationem sequi velit voluptates amet?
                                </li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>

                    <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')} sx={QuestionWrapp}>
                        <AccordionSummary
                            expandIcon={<CustomExpandIcon />}
                            aria-controls="panel2bh-content"
                            id="panel2bh-header"
                            sx={SummaryStyles}
                        >
                            <h3 className={css.questionsTitle}>Які алгоритми популярні в потоковому навчанні?</h3>
                        </AccordionSummary>
                        <AccordionDetails sx={{ padding: '0px', marginBottom: '16px' }}>
                            <ul className={css.questionsList}>
                                <li className={css.questionsItems}>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, quibusdam.
                                </li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>

                    <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')} sx={QuestionWrapp}>
                        <AccordionSummary
                            expandIcon={<CustomExpandIcon />}
                            aria-controls="panel3bh-content"
                            id="panel3bh-header"
                            sx={SummaryStyles}
                        >
                            <h3 className={css.questionsTitle}>Які припущення були при потоковому навчанні?</h3>
                        </AccordionSummary>
                        <AccordionDetails sx={{ padding: '0px', marginBottom: '16px' }}>
                            <ul className={css.questionsList}>
                                <li className={css.questionsItems}>
                                    Дані надходять по одному або невеликими партіями.
                                </li>
                                <li className={css.questionsItems}>
                                    Основний розподіл даних є стаціонарним або повільно змінюється.
                                </li>
                                <li className={css.questionsItems}>
                                    Параметри моделі вважаються стабільними, за винятком дрейфу концепції.
                                </li>
                                <li className={css.questionsItems}>
                                    Обмеженість пам'яті та обчислювальних ресурсів вимагає інкрементальних оновлень.{' '}
                                </li>
                                <li className={css.questionsItems}>Модель можна оцінювати в реальному часі.</li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>
                </div>
                <div>
                    <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')} sx={QuestionWrapp}>
                        <AccordionSummary
                            expandIcon={<CustomExpandIcon />}
                            aria-controls="panel4bh-content"
                            id="panel4bh-header"
                            sx={SummaryStyles}
                        >
                            <h3 className={css.questionsTitle}>Які алгоритми популярні в потоковому навчанні?</h3>
                        </AccordionSummary>
                        <AccordionDetails sx={{ padding: '0px', marginBottom: '16px' }}>
                            <ul className={css.questionsList}>
                                <li className={css.questionsItems}>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, quibusdam.
                                </li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>

                    <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')} sx={QuestionWrapp}>
                        <AccordionSummary
                            expandIcon={<CustomExpandIcon />}
                            aria-controls="panel5bh-content"
                            id="panel5bh-header"
                            sx={SummaryStyles}
                        >
                            <h3 className={css.questionsTitle}>Які алгоритми популярні в потоковому навчанні?</h3>
                        </AccordionSummary>
                        <AccordionDetails sx={{ padding: '0px', marginBottom: '16px' }}>
                            <ul className={css.questionsList}>
                                <li className={css.questionsItems}>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, quibusdam.
                                </li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>

                    <Accordion expanded={expanded === 'panel6'} onChange={handleChange('panel6')} sx={QuestionWrapp}>
                        <AccordionSummary
                            expandIcon={<CustomExpandIcon />}
                            aria-controls="panel6bh-content"
                            id="panel6bh-header"
                            sx={SummaryStyles}
                        >
                            <h3 className={css.questionsTitle}>Які алгоритми популярні в потоковому навчанні?</h3>
                        </AccordionSummary>
                        <AccordionDetails sx={{ padding: '0px', marginBottom: '16px' }}>
                            <ul className={css.questionsList}>
                                <li className={css.questionsItems}>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, quibusdam.
                                </li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>
                </div>
            </div>
        </Box>
    );
};

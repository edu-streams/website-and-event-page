import { Link } from 'react-router-dom';
import logo from "../../image/logo.png";

export const Logo = () => {
    return (
        <Link to='/'>
            <img src={logo} alt="logo" width={'100px'}/>
        </Link>
    )
}
import MenuIcon from '@mui/icons-material/Menu';
import { Logo } from '../Logo/Logo';
import {
    AppBar,
    Menu,
    Box,
    Button,
    Toolbar,
    // Avatar,
    Container,
    IconButton,
    // Tooltip,
    MenuItem,
    MenuList,
} from '@mui/material';

import { useState } from 'react';

const pages = ['Головна', 'Про нас', 'Стрім', 'Контакти'];
// const settings = ['Профіль', 'Акаунт', 'Logout'];

const styleTheme = {
    '&.MuiAppBar-colorPrimary': {
        color: '#18181B',
        backgroundColor: '#FFFFFF',
    },
};

const stylesItem = {
    color: '#929295',
    backgroundColor: 'transparent',
    fontWeight: 500,
    fontSize: '20px',
    display: 'block',
    '&:hover': {
        color: '#5c9726',
        backgroundColor: '#f3f3f3',
    },
    '&:focus': {
        color: '#5c9726',
        backgroundColor: '#f3f3f3',
    },
};

const buttonStyle = {
    textTransform: 'none',
    color: ' #929295',
    fontWeight: 700,
    fontSize: '14px',
    display: 'block',
    padding: '6px 15px',

    '&:hover': {
        color: '#16A34A',
        backgroundColor: 'transparent',
    },
    '&:focus': {
        color: '#16A34A',
        backgroundColor: 'transparent',
    },
};

// const iconStyle = {
//   width: { xs: '30px', md: '36px' },
//   height: { xs: '30px', md: '36px' }
// }

export const Header = () => {
    const [anchorElNav, setAnchorElNav] = useState(null);
    // const [anchorElUser, setAnchorElUser] = useState(null);

    const handleOpenNavMenu = event => {
        setAnchorElNav(event.currentTarget);
    };
    // const handleOpenUserMenu = event => {
    //     setAnchorElUser(event.currentTarget);
    // };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    // const handleCloseUserMenu = () => {
    //     setAnchorElUser(null);
    // };

    return (
        <AppBar position="static" sx={styleTheme}>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Box sx={{ flexGrow: 1 }}>
                        <Logo/>
                    </Box>
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, justifyContent: 'flex-end' }}>
                        {pages.map(page => (
                            <Button key={page} onClick={handleCloseNavMenu} sx={buttonStyle}>
                                {page}
                            </Button>
                        ))}
                    </Box>
                    <Box sx={{ flexGrow: 0, display: { xs: 'none', md: 'flex' } }}>
                        <Button
                            variant="contained"
                            sx={{ ml: '40px' }}
                        >
                            Зареєструватися
                        </Button>
                    </Box>
                    {/* АВАТАР І МЕНЮ В АВАТАРЦІ */}
                    {/* {isLoggetIn } */}
                    {/* <Box sx={{ flexGrow: 0, display: { xs: 'none', md: 'flex' } }}>
                        <Tooltip title="Open settings">
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="Avatar" src="/static/images/avatar/2.jpg" sx={{ ml: '40px' }} />
                            </IconButton>
                        </Tooltip>
                        <Menu
                            sx={{ mt: '45px' }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {settings.map(setting => (
                                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                    <Typography textAlign="center">{setting}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box> */}

                    {/* БУРГЕР КНОПКА І МЕНЮ В НІЙ */}
                    <Box sx={{ display: { xs: 'flex', md: 'none' }, justifyContent: 'flex-end' }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon sx={{ width: '30px', height: '30px' }} />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            <MenuList onClick={handleCloseNavMenu} sx={{ padding: '30px' }}>
                                <MenuItem component="a" href="/" sx={stylesItem}>
                                    Головна
                                </MenuItem>
                                <MenuItem component="a" href="/" sx={stylesItem}>
                                    Про нас
                                </MenuItem>
                                <MenuItem component="a" href="/" sx={stylesItem}>
                                    Стрім
                                </MenuItem>
                                <MenuItem component="a" href="/" sx={stylesItem}>
                                    Контакти
                                </MenuItem>
                            </MenuList>
                        </Menu>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};


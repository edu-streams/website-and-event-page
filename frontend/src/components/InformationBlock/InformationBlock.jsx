import { Typography } from '@mui/material';

import oneImg from '../../image/img_one.jpg';
import twoImg from '../../image/img_two.jpg';
import threeImg from '../../image/img_three.jpg';
import fourImg from '../../image/img_four.jpg';

import oneMini from '../../image/one_mini.jpg';
import twoMini from '../../image/two_mini.jpg';
import threeMini from '../../image/three_mini.jpg';
import fourMini from '../../image/four_mini.jpg';

import oneMaxi from '../../image/one_maxi.jpg';
import twoMaxi from '../../image/two_maxi.jpg';
import threeMaxi from '../../image/three_maxi.jpg';
import fourMaxi from '../../image/four_maxi.jpg';

import oneDesc from '../../image/one_desc.jpg';
import twoDesc from '../../image/two_desc.jpg';
import threeDesc from '../../image/three_desc.jpg';
import fourDesc from '../../image/four_desc.jpg';

import css from '../InformationBlock/InformationBlock.module.css';

export const InformationBlock = () => {
    return (
        <div className={css.mainWrapper}>
            <Typography variant="h2" component="h2" color="text.black" align="center" sx={{ fontSize: { md: '30px' } }}>
                Переваги потокового навчання
            </Typography>
            <ul className={css.list}>
                <li className={css.item}>
                    <div className={css.imgWrap}>
                        <img src={oneImg} alt="реєстрація" className={css.mobImg}/>
                        <img src={oneMini} alt="реєстрація" className={css.miniImg}/>
                        <img src={oneMaxi} alt="реєстрація" className={css.maxiImg}/>
                        <img src={oneDesc} alt="реєстрація" className={css.descImg}/>
                    </div>
                    <div className={css.textWrapper}>
                        <Typography
                            variant="subtitle"
                            component="h2"
                            color="text.black"
                            sx={{ fontSize: { md: '18px' }, marginTop:{xs: '10px', lg:'0px'} }}
                        >
                            Реєстрація
                        </Typography>

                        <Typography
                            variant="text"
                            component="p"
                            color="text.gray"
                            sx={{ marginTop: { sm: '17px', md: '20px' }, fontSize: { lg: '14px' }, width: {xs: '288px', md: '344px', lg: '357px', xl: '400px'}}}
                        >
                            Завдяки реєстрації ви зможете відкинути гру в угадайку, адже всі студенти будуть під своїми
                            іменами! І Ви зможете відмітити присутніх всього у два кліки!
                        </Typography>
                    </div>
                </li>
                <li className={css.item}>
                    <div className={css.imgWrap}>
                        <img src={twoImg} alt="опитування" className={css.mobImg}/>
                        <img src={twoMini} alt="реєстрація" className={css.miniImg}/>
                        <img src={twoMaxi} alt="реєстрація" className={css.maxiImg}/>
                        <img src={twoDesc} alt="реєстрація" className={css.descImg}/>
                    </div>
                    <div className={css.textWrapper}>
                        <Typography
                            variant="subtitle"
                            component="h2"
                            color="text.black"
                            sx={{ fontSize: { md: '18px' }, marginTop:{xs: '10px', lg:'0px'} }}
                        >
                            Опитування
                        </Typography>

                        <Typography
                            variant="text"
                            component="p"
                            color="text.gray"
                            sx={{ marginTop: { sm: '17px', md: '20px' }, fontSize: { lg: '14px' }, width: {xs: '288px', md: '344px', lg: '357px', xl: '400px'}}}
                        >
                            Для кращого засвоєння матеріалу - проводьте тестування прямо під час трансляції. Ставте
                            оцінки як під час лекції, не відриваючись від навчального процесу, так і після неї.
                        </Typography>
                    </div>
                </li>
                <li className={css.item}>
                    <div className={css.imgWrap}>
                        <img src={threeImg} alt="статистика" className={css.mobImg}/>
                        <img src={threeMini} alt="реєстрація" className={css.miniImg}/>
                        <img src={threeMaxi} alt="реєстрація" className={css.maxiImg}/>
                        <img src={threeDesc} alt="реєстрація" className={css.descImg}/>
                    </div>
                    <div className={css.textWrapper}>
                        <Typography
                            variant="subtitle"
                            component="h2"
                            color="text.black"
                            sx={{ fontSize: { md: '18px' }, marginTop:{xs: '10px', lg:'0px'} }}
                        >
                            Статистика
                        </Typography>

                        <Typography
                            variant="text"
                            component="p"
                            color="text.gray"
                            sx={{ marginTop: { sm: '17px', md: '20px' }, fontSize: { lg: '14px' }, width: {xs: '288px', md: '344px', lg: '357px', xl: '400px'}}}
                        >
                            Вся необхідна статистика за підсумками кожної лекції - допоможе оцінити кількість присутніх,
                            засвоєння матеріалу та активність студентів!
                        </Typography>
                    </div>
                </li>
                <li className={css.item}>
                    <div className={css.imgWrap}>
                        <img src={fourImg} alt="увага" className={css.mobImg}/>
                        <img src={fourMini} alt="реєстрація" className={css.miniImg}/>
                        <img src={fourMaxi} alt="реєстрація" className={css.maxiImg}/>
                        <img src={fourDesc} alt="реєстрація" className={css.descImg}/>
                    </div>
                    <div className={css.textWrapper}>
                        <Typography
                            variant="subtitle"
                            component="h2"
                            color="text.black"
                            sx={{ fontSize: { md: '18px' }, marginTop:{xs: '10px', lg:'0px'} }}
                        >
                            Увага
                        </Typography>

                        <Typography
                            variant="text"
                            component="p"
                            color="text.gray"
                            sx={{ marginTop: { sm: '17px', md: '20px' }, fontSize: { lg: '14px' }, width: {xs: '288px', md: '344px', lg: '357px', xl: '400px'}}}
                        >
                            Система автоматично проконтролює уважність студентів і не дасть їм відволіктися під час
                            навчання. Зручний таймер покаже скільки залишилося до кінця лекції та допоможе вам керувати
                            своїм часом!
                        </Typography>
                    </div>
                </li>
            </ul>
        </div>
    );
};

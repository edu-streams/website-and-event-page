import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material';
import theme from './muiTheme/theme';
import { App } from './components/App';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <CssBaseline />
            {/* додати basename до BrowserRouter */}
            <BrowserRouter>  
                <App />
            </BrowserRouter>
        </ThemeProvider>
    </React.StrictMode>
);

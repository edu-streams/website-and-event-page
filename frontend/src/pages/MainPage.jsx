import { Hero } from '../components/Hero/Hero';
import { InformationBlock } from '../components/InformationBlock/InformationBlock';
import { FAQ } from '../components/FAQ/FAQ';
import { Footer } from '../components/Footer/Footer';

const MainPage = () => {
    return (
        <>
            <Hero />
            <InformationBlock />
            <FAQ />
            <Footer />
        </>
    );
};

export default MainPage;
